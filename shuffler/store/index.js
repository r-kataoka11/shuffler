export const state = () => ({
  users: []
});

export const mutations = {
  addUser: function (state, obj) {
    state.users.unshift(obj);
  }
}
